# Vagrant Boxes

### php_imap_open.box

このボックスは、インストールが困難な、PHPの imap_open を既にインストールしてあるボックスです。
このボックスを使えば、面倒なインストールを省くことができます。

#### 元ファイル

* CentOS 6.4 x86_64

> https://github.com/2creatives/vagrant-centos/releases/download/v6.4.2/centos64-x86_64-20140116.box

#### インストール済み

```
[vagrant@vagrant-centos64 ~]$ httpd -v
Server version: Apache/2.2.15 (Unix)
Server built:   Aug 24 2015 17:52:49

[vagrant@vagrant-centos64 ~]$ php -v
PHP 5.3.3 (cli) (built: Jul  9 2015 17:39:00) 
Copyright (c) 1997-2010 The PHP Group
Zend Engine v2.3.0, Copyright (c) 1998-2010 Zend Technologies

[vagrant@vagrant-centos64 ~]$ mysql --version
mysql  Ver 14.14 Distrib 5.1.73, for redhat-linux-gnu (x86_64) using readline 5.1

```

#### 利用方法

```

vagrant box add {title} https://github.com/hagetak/vagrant_boxes/php_imap_open.box
vagrant init {title}
vagrant up

vagrant ssh

*** after vagrant ssh

[vagrant@vagrant-centos64 ~]$ php -r "echo function_exists('imap_open');"
1
[vagrant@vagrant-centos64 ~]$
```

#### 参考

* IMAP_INSTALL

> http://www.electrictoolbox.com/install-php-imap-centos/

* settings

> dotinstall( vagrant, さくらVPS)

